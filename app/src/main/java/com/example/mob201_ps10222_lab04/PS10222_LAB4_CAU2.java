package com.example.mob201_ps10222_lab04;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class PS10222_LAB4_CAU2 extends AppCompatActivity {

    TextView tv_status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ps10222__lab4__cau2);
        tv_status = (TextView) findViewById(R.id.tv_status);

        checkNetworkStatus();
    }

    public void checkNetworkStatus(){
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        boolean isWifiConn = networkInfo.isConnected();
        networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        boolean isMobileConn = networkInfo.isConnected();
        if (isWifiConn) {
            Toast.makeText(getApplicationContext(), "Thiết bị đã kết nối Wifi", Toast.LENGTH_SHORT).show();
            tv_status.setText("Đang kết nối với Wifi");
        }
        if (isMobileConn) {
            Toast.makeText(getApplicationContext(), "Thiết bị đã kết nối 3G", Toast.LENGTH_SHORT).show();
            tv_status.setText("Đang kết nối với 3G");
        }
    }
}