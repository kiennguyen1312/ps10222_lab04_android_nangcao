package com.example.mob201_ps10222_lab04;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class MainActivity extends AppCompatActivity {

    private static final int MY_PERMISSION_REQUEST_COARSE_LOCATION = 9;
    Button btnCau1, btnCau2, btnCau3, btnCau3_lamthem;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AnhXa();

        // Quyền truy cập vị trí
        requestPermission();

        btnCau1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this, PS10222_LAB4_CAU1.class);
                startActivity(intent);
            }
        });

        btnCau2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this, PS10222_LAB4_CAU2.class);
                startActivity(intent);
            }
        });

        btnCau3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this, PS10222_LAB4_CAU3.class);
                startActivity(intent);
            }
        });

        btnCau3_lamthem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this, PS10222_LAB4_CAU3_LAMTHEM.class);
                startActivity(intent);
            }
        });
    }

    // Request permission
    private void requestPermission(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSION_REQUEST_COARSE_LOCATION);
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }
    }

    public void AnhXa(){
        btnCau1 = (Button) findViewById(R.id.btnCau1);
        btnCau2 = (Button) findViewById(R.id.btnCau2);
        btnCau3 = (Button) findViewById(R.id.btnCau3);
        btnCau3_lamthem = (Button) findViewById(R.id.btnCau3_lamthem);
    }
}